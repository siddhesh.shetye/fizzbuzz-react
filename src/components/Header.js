import React from 'react';

const Header = () => {
  return (
    <div className="App">
      <div className="container pt-30 pb-30 align-self-center">
        <div className="row justify-content-center">
          <h2> FizzBuzz </h2>
        </div>
      </div>
    </div>
  );
}

export default Header;