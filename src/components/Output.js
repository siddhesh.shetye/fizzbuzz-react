import React from 'react';

const Output = () => {
  return (
    <div className="card">
      <div className="card-body">
        <p id="output" className="mr-10">  </p>
      </div>
    </div>
  );
}

export default Output;