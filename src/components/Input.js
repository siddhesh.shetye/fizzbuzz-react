import React from 'react';
import Swal from 'sweetalert2';
import Output from './Output.js';

const Input = (props) => {
    // console.log(props);

    const handleInputChange = e => {
        const { name, value } = e.target;

        props.setValues({
            ...props.values,
            [name]: value
        })
    }

    const resetForm = () => {
        props.setValues(props.initialFieldValues);
    }

    const handleSubmit = e => {
        e.preventDefault();

        if (validate()) {

            var div = document.getElementById('output');
            //remove existing elements
            while (div.hasChildNodes()) {
                div.removeChild(div.firstChild);
            }

            for (var i = 1; i <= props.values.tillCount; i++) {

                var output = "";

                if (i % props.values.fizzNumber == 0)
                    output += "Fizz"

                if (i % props.values.buzzNumber == 0)
                    output += "Buzz"

                //create new element and append the output
                var newele = document.createElement('span');
                newele.innerHTML += output ? output : i;

                //add new element
                div.appendChild(newele);
            }

            resetForm();
        }
    }

    const validate = () => {
        if (!Number.isInteger(parseInt(props.values.fizzNumber)) || props.values.fizzNumber > 100) {
            SweetResp('FizzNumber', '100');
            return false;
        }

        else if (!Number.isInteger(parseInt(props.values.buzzNumber)) || props.values.buzzNumber > 100) {
            SweetResp('BuzzNumber', '100');
            return false;
        }

        else if (!Number.isInteger(parseInt(props.values.tillCount)) || props.values.tillCount > 1000) {
            SweetResp('Till Count', '1000');
            return false;
        }

        return true;
    }

    const SweetResp = (inputName, maxDigit) => {
        Swal.fire({
            title: 'Error!',
            text: inputName + ' must be a digit only and less then ' + maxDigit + '!',
            icon: 'error',
            confirmButtonText: 'Cool'
        })
    }

    return (
        <div className="container pb-30">
            <div className="grid-margin stretch-card">
                <div className="card">
                    <form autoComplete='off' noValidate onSubmit={handleSubmit}>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="form-group row">
                                        <div className="col-sm-9">
                                            <input type="number" className="form-control" placeholder="Fizz number"
                                                name="fizzNumber" min="1" max="100"
                                                onChange={handleInputChange}
                                                value={props.values.fizzNumber} />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group row">
                                        <div className="col-sm-9">
                                            <input type="number" className="form-control" placeholder="Buzz number"
                                                name="buzzNumber" min="1" max="100"
                                                onChange={handleInputChange}
                                                value={props.values.buzzNumber} />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group row">
                                        <div className="col-sm-9">
                                            <input type="number" className="form-control" placeholder="Till which number number"
                                                name="tillCount" min="1" max="1000"
                                                onChange={handleInputChange}
                                                value={props.values.tillCount} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="card-body">
                            <button className="btn btn-primary" type="submit"> Run </button>
                        </div>
                    </form>
                </div>

                <Output />
            </div>
        </div>
    );
}

export default Input;