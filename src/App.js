import React, { useEffect, useState } from 'react';
import './App.css';
import Header from './components/Header.js';
import Input from './components/Input.js';

const initialFieldValues = {
  fizzNumber: 3,
  buzzNumber: 5,
  tillCount: 100
}

function App() {
  const [values, setValues] = useState(initialFieldValues);

  return (
    <div className="App">
      <Header />
      <Input {...{ values, setValues, initialFieldValues }} />
    </div>
  );
}

export default App;
