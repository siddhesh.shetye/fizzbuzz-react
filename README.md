This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Info
- FizzBuzz with custom user input.
- Fizz and Buzz inputs takes number from 1-100.
- TillCount is the number till which it should run and take maximum number of 1000.

## Available Scripts

In the project directory, you can run:

### `npm install`
 Run npm install command
 
 
### `npm install sweetalert2`
- Run npm install sweetalert2
- It is used to show alter message. Its kind of cool :)

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.